const minOctet = 0;
const maxOctet = 255;
const defMinPrefix = 22;
const defMaxPrefix = 30;
const defMinIP = "128.0.0.0";
const defMaxIP = "223.255.255.0";
let minPrefix = defMinPrefix;
let maxPrefix = defMaxPrefix;
let minIP = defMinIP;
let maxIP = defMaxIP;
let currentNet;
let currentSubs;

window.addEventListener("load", (event) => {
    genAssignment();
  });
  

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function divvy(number, parts, min) {

    let randombit = number - min * parts;
    let out = [];
    
    for (let i=0; i < parts; i++) {
      out.push(Math.random());
    }
    
    let mult = randombit / out.reduce(function (a,b) {return a+b;});
    
    return out.map(function (el) { return Math.floor(el * mult) + min; });
  }

function getExp(num) {
    let temp;
    let exp = 1;
    do {
        exp++;
        temp = Math.pow(2, exp);
    } while (!(temp >= num));
    return exp;
}

let CIDR2netmask = (bitCount) => {
    let mask = [];
    for(let i = 0;i < 4;i++) {
      let n = Math.min(bitCount, 8);
      mask.push(256 - Math.pow(2, 8-n));
      bitCount -= n;
    }
    return mask.join('.');
 }

function randomIP() {
    let IP = [];
    let sumIP;
    let splitMinIP = minIP.split('.');
    let splitMaxIP = maxIP.split('.');
    let minSum = Number(splitMinIP[0]) + Number(splitMinIP[1]) + Number(splitMinIP[2]) + Number(splitMinIP[3]);
    let maxSum = Number(splitMaxIP[0]) + Number(splitMaxIP[1]) + Number(splitMaxIP[2]) + Number(splitMaxIP[3]);

    do {
        do {
            for (i = 0; i < 4; i++) {
                IP[i] = getRandomInt(minOctet, maxOctet);
            }
            sumIP = Number(IP[0]) + Number(IP[1]) + Number(IP[2]) + Number(IP[3]);
        } while ((sumIP < minSum) || (sumIP > maxSum));
    } while ((Number(IP[0]) < Number(splitMinIP[0])) || (Number(IP[0]) > Number(splitMaxIP[0])));
    

    return IP.join('.');
}

function computeNextIP(ip, prefix) {
    let sNetSize = Math.pow(2, (32 - prefix));
    let splitIP = ip.split('.');
    let numCarries = [0, 0, 0, 0];

    let tempAdd = Number(splitIP[3]) + sNetSize;
    while (tempAdd > 255) {
        tempAdd = (tempAdd - 255);
        numCarries[3]++;
    }
    splitIP[3] = tempAdd;

    tempAdd = Number(splitIP[2]) + numCarries[3];
    while (tempAdd > 255) {
        tempAdd = (tempAdd - 255);
        numCarries[2]++;
    }
    splitIP[2] = tempAdd;

    tempAdd = Number(splitIP[1]) + numCarries[2];
    while (tempAdd > 255) {
        tempAdd = (tempAdd - 255);
        numCarries[1]++;
    }
    splitIP[1] = tempAdd;

    tempAdd = Number(splitIP[0]) + numCarries[1];
    while (tempAdd > 255) {
        tempAdd = (tempAdd - 255);
        numCarries[0]++;
    }
    splitIP[0] = tempAdd;

    if (numCarries[0] > 0) {
        throw "Něco se pokazilo - první pozice IP nemůže být větší než 255!"
    } else {
        return splitIP.join('.');
    }
}

function createSubs(netAddress, maxHosts) {
    let subs = [];
    let hostCounts = [];
    let prefixes = [];
    let freeSeats;
    let currentNetAddress = netAddress;

    let realHosts = getRandomInt(14, maxHosts);
    freeSeats = Number(realHosts);

    let hosts = divvy(freeSeats, 7, 2);

    for (i = 0; i < 7; i++) { 
        hostCounts.push(hosts[i]);
        let exp = getExp(hosts[i] + 2);
        prefixes.push((32 - exp));
    }

    for (i = 6; i > -1; i--) {
        subs[i] = {hostCount: hostCounts[i], netAddress: currentNetAddress, prefix: prefixes[i], netMask: CIDR2netmask(prefixes[i])};
        currentNetAddress = computeNextIP(currentNetAddress, prefixes[i]);
    }

    return subs;
}

function clear() {
    let isMask = document.getElementById("maskOrPref").checked;
    for(i = 0; i < 7; i++) {
        let netIPID = "netIP" + i;
        let maskPrefSolID = "mask-prefSol" + i;

        document.getElementById(netIPID).value = "";
        if (!isMask) {
            document.getElementById(maskPrefSolID).value = "";
        } else {
            document.getElementById(maskPrefSolID).value = "";
        }
        
    }
}

function switchLabelsAndReset() {
    let isMask = document.getElementById("maskOrPref").checked;
    if (isMask) {
        document.getElementById("mask-prefSolLabel").innerHTML = "<b>Maska podsítě</b>";
        for (let i = 0; i < 7; i++) {
            let fieldID = "mask-prefSol" + i;
            let slashes = document.getElementsByClassName("prefixSlash");
            document.getElementById(fieldID).maxlength = 15;
            document.getElementById(fieldID).size = 15;
            for (let j = 0; j < slashes.length; j++) {
                slashes[j].style.display = "none";
            }
        }
    } else {
        document.getElementById("mask-prefSolLabel").innerHTML = "<b>Prefix</b>";
        for (let i = 0; i < 7; i++) {
            let fieldID = "mask-prefSol" + i;
            let slashes = document.getElementsByClassName("prefixSlash");
            document.getElementById(fieldID).maxlength = 2;
            document.getElementById(fieldID).size = 2;
            for (let j = 0; j < slashes.length; j++) {
                slashes[j].style.display = "inline";
            }
        }
    }
    genAssignment();
}


function genAssignment() {
    let isMask = document.getElementById("maskOrPref").checked;

    clear();
    
    do {
        let netPrefix = Number(getRandomInt(minPrefix, maxPrefix));
        currentNet = {netAddress: randomIP(), prefix: netPrefix, netMask: CIDR2netmask(netPrefix), maxHosts: Math.pow(2, (32 - netPrefix))};
    } while (currentNet.maxHosts < 14);


    currentSubs = createSubs(currentNet.netAddress, currentNet.maxHosts);

    document.getElementById("netAddress").innerHTML = currentNet.netAddress;
    
    if (getRandomInt(0, 10) > 5) {
        document.getElementById("mask-prefLabel").innerHTML = "<b>Prefix</b>";
        document.getElementById("mask-pref").innerHTML = currentNet.prefix;
    } else {
        document.getElementById("mask-prefLabel").innerHTML = "<b>Maska podsítě</b>";
        document.getElementById("mask-pref").innerHTML = currentNet.netMask;
    }

    for(i = 0; i < 7; i++) {
        let hostNumID = "hostNum" + i;
        
        document.getElementById(hostNumID).innerHTML = currentSubs[i].hostCount;
    }
}

function checkAssignment() {
    let correct = true;
    let isMask = document.getElementById("maskOrPref").checked;
    for(i = 0; i < 7; i++) {
        let hostNumID = "hostNum" + i;
        let netIPID = "netIP" + i;
        let maskPrefSolID = "mask-prefSol" + i;

        let prefixIn;
        let netMaskIn;

        let netIPIn = document.getElementById(netIPID).value;
        if (!isMask) {
            prefixIn = document.getElementById(maskPrefSolID).value = currentSubs[i].prefix;
        } else {
            netMaskIn = document.getElementById(maskPrefSolID).value = currentSubs[i].netMask;
        }

        if (netIPIn != currentSubs[i].netAddress) {
            correct = false;
            break;
        }
        
        if (!isMask) {
            if (prefixIn != currentSubs[i].prefix) {
                correct = false;
                break;
            }
        } else {
            if (netMaskIn != currentSubs[i].netMask) {
                correct = false;
                break;
            }
        }
    }

    if (correct) {
        alert("Výborně! Řešení je správně.");
        genAssignment();
    } else {
        alert("Vaše řešení obsahuje jednu nebo více chyb. Zkuste to opravit");
    }
}

function autoSolve() {
    let isMask = document.getElementById("maskOrPref").checked;
    for(i = 0; i < 7; i++) {
        let netIPID = "netIP" + i;
        let maskPrefSolID = "mask-prefSol" + i;

        document.getElementById(netIPID).value = currentSubs[i].netAddress;
        if (!isMask) {
            document.getElementById(maskPrefSolID).value = currentSubs[i].prefix;
        } else {
            document.getElementById(maskPrefSolID).value = currentSubs[i].netMask;
        }
        
    }
}

function isBetween(num, min, max) {
    if (num < min) {
        return false;
    } else if (num > max){
        return false;
    } else {
        return true;
    }
}

function readSettings() {
    document.getElementById("setMinPrefix").value = minPrefix;
    document.getElementById("setMaxPrefix").value = maxPrefix;
    document.getElementById("setMinIP").value = minIP;
    document.getElementById("setMaxIP").value = maxIP;
}

function resetToDefaultSettings() {
    minPrefix = defMinPrefix;
    maxPrefix = defMaxPrefix;
    minIP = defMinIP;
    maxIP = defMaxIP;
}

function resetButtonHandler() {
    resetToDefaultSettings();
    document.getElementById("paramStatus").innerHTML = "Nastavení resetováno do výchozího stavu!";
    setTimeout(() => {  document.getElementById("paramStatus").innerHTML = "";  }, 1000);
    readSettings();
    genAssignment();
}

function writeSettings() {
    minPrefix = Number(document.getElementById("setMinPrefix").value);
    maxPrefix = Number(document.getElementById("setMaxPrefix").value);
    minIP = document.getElementById("setMinIP").value;
    maxIP = document.getElementById("setMaxIP").value;
    if ((isNaN(minPrefix)) || (isNaN(maxPrefix))){
        alert("Prefixy musí být čísla!");
        resetToDefaultSettings();
        return;
    } else if ((!isBetween(minPrefix, 1, 32)) || (!isBetween(maxPrefix, 1, 32))){
        alert("Prefixy musí být mezi 1 a 32!");
        resetToDefaultSettings();
        return;
    } else if (minPrefix > maxPrefix) {
        alert("Minimální prefix nesmí být větší než maximální!");
        resetToDefaultSettings();
        return
    } else if ((maxPrefix - minPrefix) < (32 - 25)) {
        alert("Nejmenší možný rozestup mezi minimálním a maximálním prefixem je 7!");
        resetToDefaultSettings();
        return;
    }
    document.getElementById("paramStatus").innerHTML = "Nastavení uloženo!";
    setTimeout(() => {  document.getElementById("paramStatus").innerHTML = "";  }, 1000);
    genAssignment();
}

document.onkeyup = function(e) {
    if (e.ctrlKey && e.altKey && e.shiftKey && ((e.key == "s") || (e.key == "S"))) {
        autoSolve();
    } else if (e.ctrlKey && e.altKey && e.shiftKey && ((e.key == "p") || (e.key == "P"))) {
      document.getElementById("settingsModal").style.display = "block";
      readSettings();
    } else if ((e.key == "Escape") && (document.getElementById("settingsModal").style.display == "block")){
        closeSettings();
    }
}

function closeSettings() {
    document.getElementById("settingsModal").style.display = "none";
}